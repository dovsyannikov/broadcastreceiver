package com.example.broadcastreceiverexample;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.NotificationCompat;
import androidx.localbroadcastmanager.content.LocalBroadcastManager;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.media.AudioAttributes;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {
    Button button;
    MyReceiver onNotice = new MyReceiver();
    public static final String CHANNEL_ID = "1";
    public static final int ID = 100;
    public static final String SILENT_CHANNEL_ID = "2";
    public static final int NOTIFY_ID = 101;
    NotificationCompat.Builder builder;

    PendingIntent contentIntent;
    NotificationManager nm;

    long [] longs = {0, 100, 200, 300 ,400 ,500 , 600, 700, 1000};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);


        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendMessage();
                Notification();
            }
        });




    }

    private void Notification() {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        contentIntent = PendingIntent.getActivity(this,
                ID, notificationIntent,
                PendingIntent.FLAG_CANCEL_CURRENT);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, "channelId",
                    NotificationManager.IMPORTANCE_HIGH);
            channel.setShowBadge(true);
            channel.setVibrationPattern(longs);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            notificationManager.createNotificationChannel(channel);
            builder = new NotificationCompat.Builder(this, SILENT_CHANNEL_ID);
            builder.setContentIntent(contentIntent)
                    .setSmallIcon(R.mipmap.ic_launcher)
                    .setAutoCancel(true)
                    .setContentTitle("Title")
                    .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                    .setContentText("Text, my text")
                    .addAction(R.mipmap.ic_launcher, "Action1", contentIntent);

            Notification n = builder.build();
            notificationManager.notify(NOTIFY_ID, n);
            builder = new NotificationCompat.Builder(this);

            nm = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        }




//        Intent notificationIntent = new Intent(this, MainActivity.class);
//        PendingIntent contentIntent = PendingIntent.getActivity(this, ID, notificationIntent, PendingIntent.FLAG_CANCEL_CURRENT);
//        long[] vibration = {123, 123, 0, 0, 123, 132, 0, 0,123, 123, 0, 0, 123, 132, 0, 0,123, 123, 0, 0, 123, 132, 0, 0,123, 123, 0, 0, 123, 132, 0, 0};
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
//
//            NotificationChannel channel = new NotificationChannel(SILENT_CHANNEL_ID, "Channel Id",
//                    NotificationManager.IMPORTANCE_HIGH);
//            channel.setShowBadge(true);
//            channel.setVibrationPattern(vibration);
//
//            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//            notificationManager.createNotificationChannel(channel);
//
//            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, SILENT_CHANNEL_ID);
//            builder.setContentIntent(contentIntent)
//                    .setSmallIcon(R.mipmap.ic_launcher_round)
//                    .setAutoCancel(true)
//                    .setContentTitle("Title")
//                    .setContentText("text")
//                    .setLights(Color.BLUE, 500, 500)
//                    .setVibrate(vibration)
//                    .addAction(R.mipmap.ic_launcher, "Action1", contentIntent);
//
//            Notification notification = builder.build();
//            notificationManager.notify(ID, notification);
//
//        } else {
//
//            NotificationManager notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
//
//            Notification.Builder builder = new Notification.Builder(this);
//            builder.setContentIntent(contentIntent)
//                    .setSmallIcon(R.mipmap.ic_launcher_round)
//                    .setAutoCancel(true)
//                    .setContentTitle("Title")
//                    .setContentText("text")
//                    .setLights(Color.BLUE, 500, 500)
//                    .setVibrate(vibration)
//                    .addAction(R.mipmap.ic_launcher, "Action1", contentIntent);
//            Notification notification = builder.build();
//            notificationManager.notify(ID,notification);
//        }


    }

    public void sendMessage(){
        Intent i = new Intent();
        i.setAction("broadcastreceiverexample.MyReceiver");
        LocalBroadcastManager localBroadcastManager = LocalBroadcastManager.getInstance(this);
        localBroadcastManager.sendBroadcast(i);
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(this).unregisterReceiver(onNotice);
    }

    @Override
    protected void onResume() {
        super.onResume();
        IntentFilter iff = new IntentFilter("broadcastreceiverexample.MyReceiver");
        LocalBroadcastManager.getInstance(this).registerReceiver(onNotice, iff);
    }
}
